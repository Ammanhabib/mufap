from bs4 import BeautifulSoup
import requests
import datetime
import csv
import json
def parse_rows(soup):
    table=soup.find("table",{"class":"mydata"})
    rows=table.find('tbody').findAll('tr')
    for row in rows:
        td= row.findAll('td')
        print(td)

        print(row)
        print(row.text)



def crawl_table(soup,month,year):
    table = soup.find("table", {"class": "mydata"})
    rows = table.find('tbody').findAll('tr')
    tr_list = []
    report_list = []
    for row in rows:
        check=row.attrs
        try:
            val = check['class']
        except :
            continue
        if val[0] == 'tab-headings':
            continue
        print(row.text)
        split=row.text.split('\n')
        for col in split:
            if col=="" or col == "  " or col==" ":
                continue
            else:
                tr_list.append(col)
        try:
            date=tr_list[2]
        except:
            # also apply check for grand total
            if 'Sub Total' in tr_list:
                continue


        date = date.replace(',', '')
        date=date.strip()
        dt = datetime.datetime.strptime(date,'%B %d %Y').strftime('%d-%b-%y')
        csv_row=[month,year,tr_list[0],tr_list[1],dt,tr_list[3]]
        tr_list.clear()
        report_list.append(csv_row)

    with open("MutualFunds_AUMs", "a") as f_p:
        writer = csv.DictWriter(f_p, fieldnames=['Month', 'Year', 'Fund', 'Name', 'Category', 'Inception Date', 'AUM'])
        writer.writeheader()
        writer.writerows(report_list)
        f_p.close()

    print(year, month + "done")






def crawl():
    start_year=2009
    while start_year<2020:
        for month in range(1,12):
            url = "http://www.mufap.com.pk/aum_report.php?tab=01&fname=&amc=&cat=&strdate=&endate=&submitted=&mnt=" \
                  + str(month) + "&yrs=" + str(start_year)
            r = requests.get(url)
            soup = BeautifulSoup(r.text, features="html5lib")


def year_wise():
    start_year=2009
    while start_year<2019:
        month=1
        end_year = start_year + 1
        while month<13:
            month_end=month+1
            url ="http://www.mufap.com.pk/nav-report.php?tab=01&amc=&fname=&cat=&strdate="+str(month)+"%2F02%2F"+str(start_year)+"&endate="+str(month_end)+"%2F01%2F"+str(end_year)+"&submitted=Show+Report"
            r = requests.get(url)
            soup = BeautifulSoup(r.text, features="html5lib")
            month=month+1
            parse_rows(soup)



def crawl_nav_report():
    year_wise()


def run():

    #crawl()
    crawl_nav_report()




if __name__ == '__main__':
    run()
